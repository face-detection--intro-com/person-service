package com.introcom.personservice.client;

import com.introcom.personservice.model.ChangonitionPersonModel;
import com.introcom.personservice.model.Person;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "Changonition", url = "http://localhost:8000/changonition/api/v1/person")
public interface ChangonitionClient {

    @PostMapping("/")
    public Person createPerson(@RequestBody ChangonitionPersonModel person);
    @PutMapping("/")
    public void updatePerson(@RequestBody ChangonitionPersonModel person);
    @DeleteMapping("/")
    public void deletePerson(@RequestBody ChangonitionPersonModel person);
    @PostMapping("/get_index/")
    public String getIdForFace(@RequestBody ChangonitionPersonModel person);
}
