package com.introcom.personservice.controller;

import com.introcom.personservice.client.ChangonitionClient;
import com.introcom.personservice.model.ChangonitionPersonModel;
import com.introcom.personservice.model.Person;
import com.introcom.personservice.service.PersonService;
import com.introcom.personservice.service.FileService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.PUT, RequestMethod.POST, RequestMethod.OPTIONS})
public class PersonController {

    @Autowired
    PersonService personService;
    @Autowired
    FileService fileService;
    @Autowired
    ChangonitionClient changonitionClient;

    @PostMapping("")
    @ApiOperation(value = "Add a person")
    public Person create(@RequestBody Person person){
        Person created = personService.save(person);
        changonitionClient.createPerson(new ChangonitionPersonModel(created));
        return created;
    }
    @PutMapping("")
    @ApiOperation(value = "Update a person")
    public Person update(@RequestBody Person person){
        Person updated = personService.updatePerson(person);
        changonitionClient.updatePerson(new ChangonitionPersonModel(updated));
        return updated;
    }

    @GetMapping("")
    @ApiOperation(value = "View a list of all persons registered")
    public List<Person> getAll(){
        return personService.getAll();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get a person by id")
    public Person getById(@PathVariable String id){
        return personService.getById(id);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete a person by id")
    public void deleteById(@PathVariable String id){
        personService.deleteById(id);
    }

    @PostMapping("/setImage/{id}")
    @ApiOperation(value = "Set a persons face image")
    public Person setImage(@RequestParam("file") MultipartFile file, @PathVariable String id) {
        String base64 = fileService.toBase64(file);
        Person updated = personService.setImage(id,base64);
        changonitionClient.updatePerson(new ChangonitionPersonModel(updated));
        return updated;
    }
    @GetMapping("/photo/{id}")
    @ApiOperation(value = "Get a persons photo")
    public String getPhotoForId(@PathVariable String id){
        return personService.getPhotoForId(id);
    }
}
