package com.introcom.personservice.model;

public class ChangonitionPersonModel {
    private String id;
    private String face;

    public ChangonitionPersonModel(Person p) {
        this.id = p.id;
        this.face = p.face;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }
}
