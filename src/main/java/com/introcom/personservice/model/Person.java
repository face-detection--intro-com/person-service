package com.introcom.personservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Person {

    @Id
    @ApiModelProperty(notes = "A randomly generated UUID")
    public String id;
    @ApiModelProperty(notes = "The persons name")
    public String name;
    @ApiModelProperty(notes = "A picture of the persons face")
    public String face;
    @ApiModelProperty(notes = "The last camera that saw this person")
    public Integer lastCameraSeen;
    @ApiModelProperty(notes = "The last time the person was seen in milliseconds since epoch")
    public Long lastUtcTimeSeen;


    public Person() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public Integer getLastCameraSeen() {
        return lastCameraSeen;
    }

    public void setLastCameraSeen(Integer lastCameraSeen) {
        this.lastCameraSeen = lastCameraSeen;
    }

    public Long getLastUtcTimeSeen() {
        return lastUtcTimeSeen;
    }

    public void setLastUtcTimeSeen(long lastUtcTimeSeen) {
        this.lastUtcTimeSeen = lastUtcTimeSeen;
    }
}
