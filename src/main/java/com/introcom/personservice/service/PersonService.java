package com.introcom.personservice.service;

import com.introcom.personservice.exception.NotFoundException;
import com.introcom.personservice.model.Person;
import com.introcom.personservice.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public void deletePerson(Person p) {
        personRepository.delete(p);
    }

    public void deleteById(String id) {
        if (personRepository.existsById(id)) {
            personRepository.deleteById(id);
        }
        throw new NotFoundException();
    }

    public Person save(Person p) {
        p.setId(UUID.randomUUID().toString());
        return personRepository.save(p);
    }

    public Person getById(String id) {
        return personRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public Person updatePerson(Person p) {
        if (personRepository.existsById(p.getId()))
            return personRepository.save(p);

        throw new NotFoundException();
    }

    public List<Person> getAll() {
        return personRepository.findAll();
    }

    public Person setImage(String id, String base64) {
        Person person = personRepository.findById(id).orElseThrow(NotFoundException::new);
        person.setFace(base64);
        return personRepository.save(person);
    }

    public String getPhotoForId(String id) {
        return personRepository.findById(id).map(x -> x.getFace()).orElse(null);
    }
}
